other_mad = function(z){
  return (median(abs(z - median(z))))
}

relative_med_dev <- function(x, y) {
  mad_x <- max(other_mad(x), 1e-6)
  mad_y <- max(other_mad(y), 1e-6)
  u <- ((x - median(x))/ (sqrt(2) * mad_x)) + ((y - median(y))/ (sqrt(2) * mad_y))
  v <- ((x - median(x))/ (sqrt(2) * mad_x)) - ((y - median(y))/ (sqrt(2) * mad_y))
  return(list(u=u, v=v))
}

#correlation spearman (1.1)
cor_spearman <- function(x, y){
  return(cor(x,y, method='spearman'))
}

#quadrant correlation coeffincient (1.2)
cor_Q <- function(x,y){
  return(sin(mean(sign(x - median(x)) * sign(y - median(y)))* pi / 2))
}

#MAD (1.3)
cor_MAD <- function(x, y){
  devs <- relative_med_dev(x, y)
  return((other_mad(devs$u)**2 - other_mad(devs$v)**2) / (other_mad(devs$u)**2 + other_mad(devs$v)**2))
}


# (1.4)
cor_MED <- function(x, y){
  devs <- relative_med_dev(x, y)
  return((median(abs(devs$u))**2 - median(abs(devs$v))**2) / (median(abs(devs$u))**2 + median(abs(devs$v))**2))
}

# (1.5)
cor_TRIM <- function(x, y){
  mad_x <- max(other_mad(x), 1e-6)
  mad_y <- max(other_mad(y), 1e-6)
  n <- length(x)
  n1 <- round(n * 0.2)
  n2 <- n1
  a <- ((x - median(x))/ (sqrt(2) * mad_x))
  b <- ((y - median(y))/ (sqrt(2) * mad_y))
  u <- a + b
  v <- a - b
  
  p1 <- sum(sort(u**2)[(n1 + 1):(n - n2)])
  p2 <- sum(sort(v**2)[(n1 + 1):(n - n2)])
  r <- (p1 - p2) / (p1 + p2)
  return(r)
}

# (1.6)
cor_sn <- function(x, y){
  mad_x <- max(other_mad(x), 1e-6)
  mad_y <- max(other_mad(y), 1e-6)
  a <- (x - median(x))/ (sqrt(2) * mad_x)
  b <- ((y - median(y))/ (sqrt(2) * mad_y))
  u <- a + b
  v <- a - b
  Sn_u <- Sn(u)**2
  Sn_v <- Sn(v)**2
  r <- (Sn_u - Sn_v) / (Sn_u + Sn_v)
  return(r)
}

Sn <- function(z){
  n <- length(z)
  a <- median(mapply(function(x1, i) median(abs(z[i:n] - x1)), z, seq(1:n)))
  return(a)
}

# (1.7)
cor_qn <- function(x, y){
  mad_x <- max(other_mad(x), 1e-6)
  mad_y <- max(other_mad(y), 1e-6)
  
  a <- (x - median(x))/ (sqrt(2) * mad_x)
  b <- (y - median(y))/ (sqrt(2) * mad_y)
  u <- a + b
  v <- a - b
  Qn_u <- Qn(u)**2
  Qn_v <- Qn(v)**2
  r <- (Qn_u - Qn_v) / (Qn_u + Qn_v)
  return(r)
}

Qn <- function(z){
  n <- length(z)
  h <- (n %/% 2) + 1
  k <- choose(h, 2)
  a <- as.vector(mapply(function(x1, i) abs(z[i + 1:n] - x1), z, seq(1:n)))
  a <- sort(a[!is.na(a)])
  return(a[k])
}


cor_matrix <- function(mat1, mat2, cor_fun) {
  return(apply(mat1, 2, function(cell1) apply(mat2, 2, function(cell2) cor_fun(cell1, cell2))))
}

vector_of_cor <- function(sample_1, sample_2, verbose=T){
  start.time <- Sys.time()
  cor_m_p <- cor_matrix(sample_1, sample_2, cor)
  end.time <- Sys.time()
  time.taken.p <- end.time - start.time
  if (verbose) cat("P done\n", time.taken.p, '\n')
  
  start.time <- Sys.time()
  cor_m_s <- cor_matrix(sample_1, sample_2, cor_spearman)
  end.time <- Sys.time()
  time.taken.s <- end.time - start.time
  if (verbose) cat("Spearman done\n", time.taken.s, '\n')
  
  start.time <- Sys.time()
  cor_m_q <- cor_matrix(sample_1, sample_2, cor_Q)
  end.time <- Sys.time()
  time.taken.q <- end.time - start.time
  if (verbose) cat("Q done\n", time.taken.q, '\n')
  
  start.time <- Sys.time()
  cor_m_MAD <- cor_matrix(sample_1, sample_2, cor_MAD)
  end.time <- Sys.time()
  time.taken.MAD <- end.time - start.time
  if (verbose) cat("MAD done\n", time.taken.MAD, '\n')
  
  start.time <- Sys.time()
  cor_m_MED <- cor_matrix(sample_1, sample_2, cor_MED)
  end.time <- Sys.time()
  time.taken.MED <- end.time - start.time
  if (verbose) cat("MED done\n", time.taken.MED, '\n')
  
  start.time <- Sys.time()
  cor_m_TRIM <- cor_matrix(sample_1, sample_2, cor_TRIM)
  end.time <- Sys.time()
  time.taken.TRIM <- end.time - start.time
  if (verbose) cat("TRIM done\n", time.taken.TRIM, '\n')
  
  start.time <- Sys.time()
  cor_m_sn <- cor_matrix(sample_1, sample_2, cor_sn)
  end.time <- Sys.time()
  time.taken.sn <- end.time - start.time
  if (verbose) cat("SN done\n", time.taken.sn, '\n')
  
  start.time <- Sys.time()
  cor_m_qn <- cor_matrix(sample_1, sample_2, cor_qn)
  end.time <- Sys.time()
  time.taken.qn <- end.time - start.time
  if (verbose) cat("QN done\n", time.taken.qn, '\n')
  
  return(list(list(Pearson=cor_m_p, Spearman=cor_m_s, Q=cor_m_q, MAD=cor_m_MAD, MED=cor_m_MED, TRIM=cor_m_TRIM, Sn=cor_m_sn, Qn=cor_m_qn),
  list(Pearson=time.taken.p, Spearmen=time.taken.s, Q=time.taken.q, MAD=time.taken.MAD, MED=time.taken.MED, TRIM=time.taken.TRIM, Sn=time.taken.sn, Qn=time.taken.qn)))
}
